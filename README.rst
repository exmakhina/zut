######################
zut (is a useful tool)
######################

You should not bother with zut but use https://github.com/casey/just

zut is a productivity tool allowing to run short scripts ("zut
files") doing anything.
When the current working directory lies under a directory containing a
zut file, running zut will call commands contained in the zut file.


Usage
#####

Examples:

- Create a ``Makefile.zut`` in ~/path/to/project, and when the working
  directory is balow, doing ``zut x`` will call the `x` target in that
  makefile.

  In the case of makefiles, the subprocess cwd is set to be in the
  containing folder.

- Create a ``zut`` file with a shebang and zut will find its
  interpreter and call it, to use the script's code and add an entry
  point calling functions.

  For now only shell scripts are supported.
  When a script is called, cwd is not changed.
